package android.example.com.slidingmenuexample.fragment;

import android.example.com.slidingmenuexample.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by yodgorbek on 2016-03-25.
 */
public class Fragment3 extends Fragment {
    public Fragment3() {

    }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
            View rootView = inflater.inflate(R.layout.fragment3, container, false);
            return rootView;

    }


}
