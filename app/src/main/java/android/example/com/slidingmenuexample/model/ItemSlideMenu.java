package android.example.com.slidingmenuexample.model;

/**
 * Created by yodgorbek on 2016-03-25.
 */
public class ItemSlideMenu {

    private int imgId;
    private String title;

    public int getImgId() {
        return imgId;
    }

    public ItemSlideMenu(int imgId, String title) {
        this.imgId = imgId;
        this.title = title;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
