package android.example.com.slidingmenuexample.fragment;

import android.app.Fragment;
import android.example.com.slidingmenuexample.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by yodgorbek on 2016-03-25.
 */
public class Fragment1 extends Fragment {
   public Fragment1(){

   }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment1, container, false);
        return rootView;

    }

}
